import moment from 'moment';
import 'moment/locale/es'
import transformWeather from './transformWeather';

const transformForecast = data => (
    data.list.filter(item => (
        item.dt_txt[11] === "0" & item.dt_txt[12] === "6" ||
        item.dt_txt[11] === "1" & item.dt_txt[12] === "2" ||
        item.dt_txt[11] === "1" & item.dt_txt[12] === "8" 
    )).map(item => (
        {
            weekDay: moment.unix(item.dt).format('ddd'),
            hour: moment.unix(item.dt).hour()-1,
            data: transformWeather(item)
        }
    ))
);



export default transformForecast;